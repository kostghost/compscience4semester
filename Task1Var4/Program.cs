﻿/*
Список соседей -> Матрица инцидентности
Вид изначального списка смежности (пример)
ПЛОХАЯ РЕАЛИЗАЦИЯ - остаются храниться вершины без связей.
0 - 1 2
1 - 0 2 3
2 - 0 1 3 4 7
3 - 1 2 5
4 - 2
5 - 3
7 - 2

Вид результата (вершины по строчкам, ребра  по столбцам)

1100000 (трансп.)
1011000
0110110
0001101
0000010
0000001

*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1Var4
{
    class GraphRepresentations
    {
        //считаем количетсво ребер в списках соседей
        static int CalculateEdgesCount(List<List<int>> neighboursList)
        {
            int count = 0;
            foreach (List<int> list in neighboursList) //сначала посчитем, сколько связей есть в наших списках (их всегда четно, т.к. связи повторяются)
            {
                count += list.Count();
            }
            return count / 2;  // и разделим на 2 - получим количество ребер.
        }

        static void PrintMatrixToConsole(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j]);
                }
                Console.WriteLine();
            }
        }

        //Исправлено.
        static List<List<int>> ReadNeighboursFromFile()
        {
            List<List<int>> neighboursList = new List<List<int>>();

            using (StreamReader sr = new StreamReader("input.txt"))
            {
                string line; //Держим прочитанную строку
                string[] lineArray;
                while ((line = sr.ReadLine()) != null)
                {
                    lineArray = line.Split(new char[] { ' ' , '-' }, StringSplitOptions.RemoveEmptyEntries); //разбиваем строку на слова, удаляя пробелы и -
                    int n = int.Parse( lineArray[0]); // строчка, в которую будем добавлять соседей

                    while (neighboursList.Count() - 1 < n) //проверяем, хватает ли строчек для вставки
                        neighboursList.Add(new List<int>()); //если не хватает, добиваем пустыми списками

                    //без проверки!
                    for (int i = 1; i < lineArray.Count(); i++) //вставляем новых соседей (начиная со второго элемента строчки )
                    {
                        neighboursList.ElementAt(n).Add(int.Parse(lineArray[i])); 
                    }         
                }               
            }
            return neighboursList;
        }

        //Список соседей -> Матрица инцидентности
        static int[,] NeighboursToIncidence(List<List<int>> neighboursList)
        {
            int edgesCount = CalculateEdgesCount(neighboursList);
            int vertexCount = neighboursList.Count();

            int[,] incidenceMatrix = new int[edgesCount, vertexCount];


            /* Нужно убрать повторы. Сыграем на том, что если n > k, то не записываем его. Этим пройдем только по элементам под главной диагональю */
            int currentEdge = 0;
            for (int i = 0; i < neighboursList.Count(); i++)
            {
                for (int j = 0; j < neighboursList.ElementAt(i).Count(); j++)
                {
                    if (i <= neighboursList.ElementAt(i).ElementAt(j))
                    {
                        //Тут ребро currentEdge связывает вершины i и neighboursList.ElementAt(i).ElementAt(j), так что выделяем их 1-ми
                        incidenceMatrix[currentEdge, neighboursList.ElementAt(i).ElementAt(j)] = 1;
                        incidenceMatrix[currentEdge, i] = 1;                 
                        currentEdge++;
                    }
                }
            }
            /* endof СЛишком сложнааааа */
            return incidenceMatrix;
        }

        static void PrintNeighboursList(List<List<int>> neighboursList)
        {
            int i = 0;
            foreach (List<int> list in neighboursList) //сначала посчитем, сколько связей есть в наших списках (их всегда четно, т.к. связи повторяются)
            {
                if (list.Count() > 0)
                {
                    Console.Write(i);
                    Console.Write(" - ");
                    foreach (int num in list)
                    {
                        Console.Write(num);
                        Console.Write(" ");
                    }
                    Console.WriteLine();
                }
                i++;
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            List<List<int>> neighboursList = ReadNeighboursFromFile();
            int[,] incidenceMatrix = NeighboursToIncidence(neighboursList);

            PrintNeighboursList(neighboursList);

            PrintMatrixToConsole(incidenceMatrix);

            Console.ReadKey();
        }
    }
}
