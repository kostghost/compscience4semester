#include <iostream>
#include <vector>
#include <algorithm>

const int VERTEX_COUNT = 5;
const int MAXN = 50; //������������ ����� ������ � �������� �����
//��� ���� - ������ �������� ������ (������ ���������)
std::vector<int> g[VERTEX_COUNT] = {
	std::vector<int>{1, 2},
	std::vector<int>{0, 2},
	std::vector<int>{0, 1, 3},
	std::vector<int>{2, 4, 5},
	std::vector<int>{3, 4}
};
bool used[MAXN]; //���������� ������� ???
int timer; //��������� �� ��������� �������
int tin[MAXN]; //����� ������ ������ � ������� � �������
int fup[MAXN];

void PrintBridge(int from, int to) {
	std::printf("(%d, %d) ", from, to);
}

//v - �������, p - ������������� �������
void Dfs(int v, int p = -1) {
	used[v] = true;  //������ ���������� ������ v
	tin[v] = fup[v] = timer++;

	for (size_t i = 0; i<g[v].size(); ++i) {
		int to = g[v][i];
		if (to == p)  continue;
		if (used[to])
			fup[v] = std::min(fup[v], tin[to]);
		else {
			Dfs(to, v);
			fup[v] = std::min(fup[v], fup[to]);
			if (fup[to] > tin[v])
				PrintBridge(v, to);
		}
	}
}

void FindBridges() {

	timer = 0;
	//�� �������� ������
	for (int i = 0; i<VERTEX_COUNT; ++i)
		used[i] = false;

	//��������� ������ � �������
	for (int i = 0; i<VERTEX_COUNT; ++i)
		if (!used[i])
			Dfs(i);
}

int main(){

	FindBridges();

	system("pause");
	return 0;
}