﻿/*
https://github.com/serra/QuickgraphExamples/blob/master/src/examples/FindAPath.cs - пример
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;
using QuickGraph.Algorithms;
using QuickGraph.Graphviz;
using System.Drawing;
using System.Diagnostics;
using QuickGraph.Graphviz.Dot;
using System.IO;

namespace Task2
{
    class Program
    {
        static Dictionary<TaggedEdge<int, string>, double> edgeCost; //Вес ребра
        static Random rnd = new Random(); //Определяем генератор случайных чисел        
        static int VERTEXCOUNT = 20;
        static int EDGEPERCENT = 20; //примерный процент граней относительно количества всех возможных
        static int MAXWEIGHT = 100;

        private static void FormatVertex(object sender, FormatVertexEventArgs<int> e)
        {
            e.VertexFormatter.Label = e.Vertex.ToString();
            e.VertexFormatter.Shape = QuickGraph.Graphviz.Dot.GraphvizVertexShape.Box;
            //e.VertexFormatter.StrokeColor = Color.Yellow;
            //e.VertexFormatter.Font = new Font(FontFamily.GenericSansSerif, 12);
        }

        private static void FormatEdge(object sender, FormatEdgeEventArgs<int, TaggedEdge<int, string>> e)
        {
            e.EdgeFormatter.Label.Value = edgeCost[e.Edge].ToString("0.0");

            //e.EdgeFormatter.Tail.Label = e.Edge.Source;
            //e.EdgeFormatter.Font = new Font(FontFamily.GenericSansSerif, 8);
            //e.EdgeFormatter.FontColor = Color.Red;
            //EdgeFormatter.StrokeColor = Color.Gray;
            //e.EdgeFormatter.Label = "ad";
        }


        public sealed class FileDotEngine : IDotEngine
        {
            public string Run(GraphvizImageType imageType, string dot, string outputFileName)
            {
                string output = outputFileName;
                File.WriteAllText(output, dot);

                // assumes dot.exe is on the path:
                var args = string.Format(@"{0} -Tjpg -O", output);
                System.Diagnostics.Process.Start(@"C:\Program Files (x86)\Graphviz2.38\bin\dot.exe", args);
                return output;
            }
        }

        public sealed class DotPrinter : IDotEngine
        {
            public string Run(GraphvizImageType imageType, string dot, string outputFileName)
            {
                return dot;
            }
        }


        
        static void Main(string[] args)
        {
            if (args.Count() == 3)
            {
                VERTEXCOUNT =   int.Parse( args[0] );
                EDGEPERCENT =   int.Parse( args[1] );
                MAXWEIGHT =     int.Parse( args[2] );

            }
         //*****GraphCreating*******
            var graph = new AdjacencyGraph<int, TaggedEdge<int, string>>();

            //Создаем вершины с цифровыми идентификаторами, с 0 по VERTEXCOUNT
            List<int> vertexes = new List<int>();
            for (int i = 0; i < VERTEXCOUNT; i++)
               vertexes.Add(i);
            graph.AddVertexRange(vertexes);

            //Создаем  случайные ребра, направленные с меньшей вершины к большей
            List<TaggedEdge<int, string>> edges = new List<TaggedEdge<int, string>>();
            for (int i = 0; i < VERTEXCOUNT; i++)
                for (int j = i+1; j < VERTEXCOUNT; j++)
                {
                    if (rnd.Next() % (100 / EDGEPERCENT) == 0) 
                        edges.Add(new TaggedEdge<int, string>(i, j, String.Format("{0}_{1}",i, j) ));
                }
            graph.AddEdgeRange(edges);

            

            // Определяем вес ребер (рандомно)
            edgeCost = new Dictionary<TaggedEdge<int, string>, double>(graph.EdgeCount);
            foreach (TaggedEdge<int, string> edge in edges)
            {
                edgeCost.Add(edge, rnd.Next() % MAXWEIGHT);
            }
                       
            //Console.WriteLine("Topological Sorting...");
            //foreach (var elem in graph.TopologicalSort())
            //{
            //    Console.WriteLine(elem);
            //}


        //******* Graph Visualizing *********

            var graphViz = new GraphvizAlgorithm<int, TaggedEdge<int, string>>(graph, @".\", QuickGraph.Graphviz.Dot.GraphvizImageType.Png);
            graphViz.FormatVertex += FormatVertex;
            graphViz.FormatEdge += FormatEdge;
            graphViz.Generate(new FileDotEngine(), "myGraph");
           // Console.ReadLine();

        }



    }
}

