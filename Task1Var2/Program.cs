﻿/*
Вид изначальной матрицы смежности (пример)

5
0 0 0 1 0
0 0 1 0 0
0 1 0 0 0
1 0 0 0 1
0 0 0 1 0

*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1Var2
{
    class GraphRepresentations
    {
            
        static bool[,] ReadAdjacencyFromFile()
        {
            bool[,] adjacecyMatrix;
            using (StreamReader sr = new StreamReader("input.txt"))
            {
                char lastCh; //Держим последний прочитаный символ
                int n = sr.Read() - '0'; //считываем количество элементов матрицы
                adjacecyMatrix = new bool[n, n]; //создаем массив для хранения

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {

                        do //Читаем пока не встретим 0 или 1
                        {
                            lastCh = (char)sr.Read();
                        } while (lastCh != '0' && lastCh != '1');

                        adjacecyMatrix[i, j] = lastCh == '0' ? false : true;
                    }
                }

                return adjacecyMatrix;
            }
        }

        static void WriteMatrixToConsole(bool[,] matrix)
        {
           for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == true)
                        Console.Write("1 ");
                    else
                        Console.Write("0 ");
                    
                }
                Console.WriteLine();
            }
        }

        static bool [,] AdjacencyToIncidence(bool[,] adjMatrix)
        {

            int vertexCount = adjMatrix.GetLength(0);
            int edgesCount = EdgesCount(adjMatrix);
            bool[,] incMatrix = new bool[edgesCount,vertexCount];

            int edge = 0;

            for (int i = 0; i < vertexCount; ++i)
            {
                for (int j = 0; j <= i; ++j)
                {
                    if (adjMatrix[i, j])
                    {
                        incMatrix[edge, j] = incMatrix[edge, i] = true;
                        ++edge;
                    }
                }
            }

            return incMatrix;
        }

        static bool [,] IncidenceToAdjacency(bool[,] incMatrix)
        {
            int edges = incMatrix.GetLength(0);
            int vertices = incMatrix.GetLength(1);

            bool[,] adjMatrix = new bool [vertices, vertices];

            for (int edge = 0; edge<edges; ++edge)
            {
                int a = -1, b = -1, vertex = 0;

                for (; vertex<vertices && a == -1; ++vertex) //ищем координату a
                {
                    if (incMatrix[edge, vertex])
                        a = vertex;
                }

                for (; vertex<vertices && b == -1; ++vertex) //ищем координату b
                {
                    if (incMatrix[edge, vertex])
                        b = vertex;
                }

                if (b == -1) //если петля
                    b = a;
                adjMatrix[a, b] = adjMatrix[b, a] = true;
            }

            return adjMatrix;
        }

        static int EdgesCount(bool[,] adjMatrix)
        {
            int edgesCount = 0;
            int vertexCount = adjMatrix.GetLength(0);
            for (int i = 0; i < vertexCount; ++i)
            {
                for (int j = 0; j <= i; ++j)
                {
                    if (adjMatrix[i, j])
                    {
                        edgesCount++;
                    }
                }
            }
            return edgesCount;
        }

        static void Main(string[] args)
        {
            bool[,] adjMatrix = ReadAdjacencyFromFile();
            bool[,] incMatrix = AdjacencyToIncidence(adjMatrix);
            WriteMatrixToConsole(adjMatrix);
            Console.WriteLine();
            WriteMatrixToConsole(incMatrix);

            Console.WriteLine();
            adjMatrix = IncidenceToAdjacency(incMatrix);
            WriteMatrixToConsole(adjMatrix);

            Console.ReadKey();
        }
    }
}
