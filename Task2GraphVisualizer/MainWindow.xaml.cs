﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Task2GraphVisualizer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var args = string.Format(@"{0} {1} {2}", textBox0.Text, textBox1.Text, textBox2.Text);
            System.Diagnostics.Process.Start(@"..\..\..\Task2GraphGenerator\bin\Debug\Task2GraphGenerator.exe", args);
            
            FileStream file = new FileStream("myGraph.jpg", FileMode.Open);
            BitmapDecoder bitmap = BitmapDecoder.Create(file, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            this.image1.Source = bitmap.Frames[0];
            file.Close();

          
        }
    }
}
